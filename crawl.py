import bs4
import requests
import collections
from urllib.parse import urljoin

ZH_MAIN_URL = "https://www.zhihu.com/"
DEFAULT_USER_AGENT_FOR_CRAWLER = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:52.0) Gecko/20100101 Firefox/52.0"
IMAGE_ATTR = {"class": "origin_image"}
ORIGINAL_ATTR_NAME = 'data-original'


class DownloadFailedException(Exception):
    def __init__(self, *args):
        super().__init__(*args)


class Crawler:
    def __init__(self):
        self.sess = requests.Session()
        self.sess.headers['User-Agent'] = DEFAULT_USER_AGENT_FOR_CRAWLER
        self.done = set([])

    def download(self, url):
        filename = url.split('/')[-1]
        response = self.sess.get(url)
        if response.status_code >= 400:
            raise DownloadFailedException(filename)
        return filename, response.content

    def crawl(self, start_url):
        queue = collections.deque()
        queue.append(start_url)
        while len(queue) > 0:
            start_url = queue.popleft()
            print('Visiting: ' + start_url)
            if start_url in self.done:
                continue
            self.done.add(start_url)
            response = self.sess.get(start_url)
            if response.status_code >= 400:
                continue
            soup = bs4.BeautifulSoup(response.text, 'lxml')
            for img in soup.find_all('img', IMAGE_ATTR):
                if 'lazy' not in img.attrs.get('class'):
                    yield img.attrs.get(ORIGINAL_ATTR_NAME)
            for a in soup.find_all('a'):
                href = a.attrs.get('href')
                if href is None:
                    continue
                ok = False
                ok = True if '/question/' in href else ok
                ok = True if '/collection/' in href else ok
                ok = True if 'people' in href else ok
                ok = False if 'mailto' in href else ok
                if ok:
                    queue.append(urljoin(ZH_MAIN_URL, href))
