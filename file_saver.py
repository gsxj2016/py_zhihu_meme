import os

MEME_DIR = 'meme_image'
OTHER_DIR = 'other_image'


def _create_dir(dir_name):
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)


def init_dirs():
    _create_dir(MEME_DIR)
    _create_dir(OTHER_DIR)


def save_meme(file_name, data):
    with open(MEME_DIR + '/' + file_name, 'wb') as f:
        f.write(data)


def save_other(file_name, data):
    with open(OTHER_DIR + '/' + file_name, 'wb') as f:
        f.write(data)
