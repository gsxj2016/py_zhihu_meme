import crawl
import file_saver
import checker

START_URL = "https://www.zhihu.com/topic/20037934/hot"

if __name__ == "__main__":
    file_saver.init_dirs()
    c = crawl.Crawler()
    for img_path in c.crawl(START_URL):
        print(img_path)
        file_name, data = c.download(img_path)
        if checker.is_meme(data):
            file_saver.save_meme(file_name, data)
        else:
            file_saver.save_other(file_name, data)
