import numpy as np
import cv2
import clustering


def show_histogram(input_img, histogram_name='histogram'):
    hist = cv2.calcHist([input_img], [0], None, [256], [0, 255])
    histimg = np.zeros([256, 256, 3], np.uint8)
    hpt = int(0.9 * 256)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(hist)
    for h in range(256):
        intensity = int(hist[h] * hpt / max_val)
        cv2.line(histimg, (h, 256), (h, 256 - intensity), [255, 255, 255])

    cv2.imshow(histogram_name, histimg)
    return hist


def edge_canny(input_img):
    temp_img = cv2.GaussianBlur(input_img, (3, 3), 0)
    result = cv2.Canny(temp_img, 1, 40)
    #    cv2.imshow('edge_canny', result)
    non_zero_count = 0
    for lines in result:
        for pixels in lines:
            if pixels != 0:
                non_zero_count += 1
    return non_zero_count / len(result) / len(result[0])


def is_meme(input_img):
    ttl_size_x = len(input_img)
    ttl_size_y = len(input_img[0])
    # cv2.imshow('pic', input_img)
    if ttl_size_x > 1024 and ttl_size_y > 1024:
        return False
    edge_point_percentage = edge_canny(input_img)
    point_list = []
    mul = (ttl_size_x * ttl_size_y) // 6400
    if mul < 1:
        mul = 1
    for i in range(ttl_size_x // mul):
        for j in range(ttl_size_y // mul):
            point_list.append(input_img[mul * i][mul * j])
    delta = clustering.judge_by_color(point_list, 3)
    #    print(edge_point_percentage)
    #    print(delta)
    epp_thre = 0.15
    delta_thre = 48
    return (edge_point_percentage < epp_thre) and (delta < delta_thre)
