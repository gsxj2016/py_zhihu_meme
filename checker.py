import cv2
import numpy
import get_meme_picture


def is_meme(data):
    img = cv2.imdecode(numpy.asarray(bytearray(data)), cv2.IMREAD_COLOR)
    return get_meme_picture.is_meme(img)
