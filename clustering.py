import random
import math


def distance(_po, _ce):
    r_mean = (_po[2] + _ce[2]) / 2
    d_r2 = (_po[2] - _ce[2]) ** 2
    d_g2 = (_po[1] - _ce[1]) ** 2
    d_b2 = (_po[0] - _ce[0]) ** 2
    d_c = math.sqrt((2 + r_mean / 256) * d_r2 + 4 * d_g2 + (2 + (255 - r_mean) / 256) * d_b2)
    return d_c


def get_centroid(_point_list, _category_list, k):
    category_count = [0 for x in range(0, k)]
    category_total = [[0, 0, 0] for x in range(0, k)]
    for i in range(len(_point_list)):
        category_count[_category_list[i]] += 1
        for channel in range(0, 3):
            category_total[_category_list[i]][channel] += _point_list[i][channel]
    for i in range(k):
        for channel in range(3):
            if category_count[i]:
                category_total[i][channel] /= category_count[i]
    return category_total


def classify(_point_list, _centroid_list, k):
    category_id = [-1 for x in range(0, len(_point_list))]
    for i in range(len(_point_list)):
        ans_id = -1
        ans_dis = 1000000000
        for cenID in range(k):
            dis = distance(_point_list[i], _centroid_list[cenID])
            if dis < ans_dis:
                ans_dis = distance(_point_list[i], _centroid_list[cenID])
                ans_id = cenID
        category_id[i] = ans_id
    return category_id


def k_means_cluster(point_list, k):
    final_category = [-1 for x in range(len(point_list))]
    centroid_list = [[random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)] for x in range(k)]
    for iteration in range(8):
        final_category = classify(point_list, centroid_list, k)
        centroid_list = get_centroid(point_list, final_category, k)
    return final_category, centroid_list


def judge_by_color(point_list, k):
    (pointCategory, centroid) = k_means_cluster(point_list, k)
    ave_delta = 0
    delta = [0 for x in range(len(point_list))]
    for i in range(len(point_list)):
        delta[pointCategory[i]] += distance(point_list[i], centroid[pointCategory[i]])
    for i in range(k):
        ave_delta += delta[i]
    ave_delta /= len(point_list)
    return ave_delta
